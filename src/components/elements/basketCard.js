import './basketCard.css'

function BasketCard({img, name, price}) {
  return (
    <div className="basket-card">
      <div className="basket-card__wrap">
        <img className='basket-card__img' src={img} alt="basket__img"/>
        <h2 className="basket-card__title">
            {name}
        </h2>
      </div>
      <div className="basket-card__price">
          <div className="basket-card__tag">
              {price} ₽
          </div>
          <img className='basket-card__cancel' src="./images/cancel.svg" alt="cancel"/>
      </div>
    </div>
  );
}

export default BasketCard;