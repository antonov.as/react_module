import './card.css';

function Card({img, name, descriprion, price, weight}) {
  return (
    <div className='card'>
      <img className='card__preview' src={img} alt='plate'></img>
      <h2 className='card__title'>
        {name}
      </h2>
      <div className='card__description'>
        {descriprion}
      </div>
      <div className='card__price'>
        <div className='price'>
          {price}<span> / {weight}</span> 
        </div>
        <div className='plus'>+</div>
      </div>
    </div>
  );
}

export default Card;