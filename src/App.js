import './App.css';
import './basket.css';
import BasketCard from './components/elements/basketCard.js';
import {basketList} from './basketList';


// import Card from './components/elements/card.js';
// import {menuList} from './menuList.js';

function App() {
  return (
    // <main className='main'>
    //   <div className='container'>
    //     <header className='header'>
    //       <h1 className='header__title'>
    //         наша продукция
    //       </h1>
    //       <div className='header__basket'>
    //         <p className='basket__text'>
    //           3 товара <br></br>на сумму 3 500 ₽
    //         </p>
    //         <img src="./images/basket.svg" alt="basket" />
    //       </div>
    //     </header>
    //     <div className='menu'>
    //       {menuList.map(key => {
    //         return (
    //           <Card
    //             img={key.img}
    //             name={key.name}
    //             descriprion={key.description}
    //             price={key.price}
    //             weight={key.weight}
    //           />
    //         )
    //       })}
    //     </div>
    //   </div>
    // </main>
    <div className="basket">
      <div className="container">
        <div className="basket__head">
          <img src="./images/arrow.svg" alt="arrow" />
          <h2 className="basket__title">
            Корзина с выбранными товарами
          </h2>
        </div>
        <div className="basket__list">
          {basketList.map(key =>{
            return (
              <BasketCard
                img={key.img}
                name={key.name}
                price={key.price}
              />
            )
          })}
        </div>
      </div>
      <footer className='basket__order'>
        <div className="container">
          <div className="order__wrap">
            <div className="order__title">
              Заказ на сумму:
            </div>
            <div className="order__price">
              6 220 ₽
            </div>
            <button className='order__btn'>
              Оформить заказ
            </button>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default App;
